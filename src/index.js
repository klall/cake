'use strict';

const { Heap } = require('heap-js');

/**
 * Find the K-th piece of cake in order of size, starting with the largest piece first.
 *
 * @public
 *
 * @param {number}   X Cake length of the first side
 * @param {number}   Y Cake length of the second side
 * @param {number}   K Which Kth largest piece of cake to find
 * @param {number[]} A Cut positions along first side
 * @param {number[]} B Cut positions along second side
 *
 * @return {number[]} The size of the Kth's largest piece of cake
 */
function solution(X, Y, K, A, B) {
  _validateInput(X, Y, K, A, B);

  const heap = new Heap(Heap.maxComparator);

  let cutLengthsA = [ ], // Lengths of cuts on first side
      cutLengthsB = [],  // Lengths of cuts on second side
      cutLength = 0;     // The length of a cut

  // Collect the cut lengths on the first side
  A.forEach((cutPosition)=> {
    cutLength = cutPosition - cutLength;
    cutLengthsA.push(cutLength);
  });

  cutLength = X - A[A.length - 1];
  cutLengthsA.push(cutLength);

  // Note: Technically we don't need to sort here since the
  // heap keeps things in order but apparently it is faster
  // on my machine to perform a sort first.
  // Without a sort, Scenario 3 test case takes 428ms
  // With pre-sorting, Scenario 3 test case takes 350ms
  cutLengthsA = cutLengthsA.sort(_sortNumber).reverse();

  // Collect the cut lengths on the second side
  cutLength = 0; // Reset cut length
  B.forEach((cutPosition)=> {
    cutLength = cutPosition - cutLength;
    cutLengthsB.push(cutLength);
  });

  cutLength = Y - B[B.length - 1];
  cutLengthsB.push(cutLength);
  cutLengthsB = cutLengthsB.sort(_sortNumber).reverse();

  // Place everything in a max heap.
  for (let i = 0; i < cutLengthsA.length; ++i) {
    heap.push(cutLengthsA[i] * cutLengthsB[0]);
  }

  // TODO: Should be able to optimize here to return
  // the kth's largest size within the loop.
  for (let i = 1; i < cutLengthsA.length; ++i) {
    for (let j = 0; j < cutLengthsB.length; ++j) {

      heap.push(cutLengthsA[j] * cutLengthsB[i]);
    }
  }

  let size = heap.pop(),
      count = 1;
  while (size = heap.pop()) {
    if (++count === K) {
      return size;
    }
  }

  return size;
}

/**
 * Number comparator used for sorting.
 *
 * @param  {number} a Number
 * @param  {number} b Number
 * @return {number} The result of the comparison
 */
function _sortNumber(a, b) {
  return a - b;
}

/**
 * Validate input.
 *
 * @param {number}   X Cake length of the first side
 * @param {number}   Y Cake length of the second side
 * @param {number}   K Which Kth largest piece of cake to find
 * @param {number[]} A Cut positions along first side
 * @param {number[]} B Cut positions along second side
 */
function _validateInput(X, Y, K, A, B) {
  if (X < 2 || X > 400000000) {
    throw new Error('Invalid input value X', X);
  }
  if (Y < 2 || Y > 400000000) {
    throw new Error('Invalid input value X', X);
  }

  // TODO: Could potentially validate the following
  // depending on API requirements
  // Constraints on N
  // each element of array A is an integer within the range [1..X−1];
  // each element of array B is an integer within the range [1..Y−1];
  // A[I − 1] < A[I] and B[I − 1] < B[I], for every I such that 0 < I < N;
  // 1 ≤ A[I] − A[I − 1], B[I] − B[I − 1] ≤ 10,000, for every I such that 0 < I < N;
  // 1 ≤ A[0], B[0], X − A[N − 1], Y − B[N − 1] ≤ 10,000.
}

module.exports = solution;
