'use strict';

const path = require('path'),
      chai = require('chai'),
      sinonChai = require('sinon-chai');

// Install plugins
chai.use(sinonChai);

/**
 * Resolve given a root directory.
 * @param  {string} root root directory
 * @return {string} the resolved path
 */
function pathResolver(root) {
  return {
    root: root,

    join(p) {
      return path.join(this.root, p);
    },

    require(p) {
      return require(this.join(p));
    }
  };
}

exports.src = pathResolver(path.resolve(path.join(__dirname, '..', 'src')));
exports.test = pathResolver(path.resolve(path.join(__dirname)));

