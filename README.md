# Cake Piece Finder 

# Overview 

Find the K-th piece of cake in order of size, starting with the largest piece first.  

# Running Integration Tests

Run: 

```shell
npm run itest
```

If you wish to also include code coverage information, then run: 

```shell
npm run itestc
```

It is recommended that you run tests in TDD mode to autorun tests as you change code. 

```shell
npm run itestw 

# Linting Code

```shell
npm run lint
```

If you wish to use eslint's '--fix' feature, you can run:

```shell
npm run lint:fix
```
